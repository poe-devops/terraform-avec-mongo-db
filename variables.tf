variable "container_name_mongodbserver" {
  default     = "my_mongodbserver"
}

variable "container_port_mongodbserver" {
  default     = 27017
}

variable "mongodb_uri" {
  default = "mongodb://my_mongodbserver:27017/mydatabase"
}
