resource "docker_image" "mongodbserver" {
    name = "mongo:latest"
}

resource "docker_container" "mongodbserver" {
    image = docker_image.mongodbserver.latest
    name = var.container_name_mongodbserver
    ports {
        internal = 27017
        external = var.container_port_mongodbserver
    } 

    mounts {
      type = "volume"
      source = "db_data"
      target = "/data/db"
    }

    env = [
    "MONGODB_URI=${var.mongodb_uri}"
    ]

}
